import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { Login } from '../../models/login';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { HomePage } from '../../pages/home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  login: Login;

  constructor(public navCtrl: NavController, private authService: AuthServiceProvider, private toast: ToastController) {
    this.login = { email: '', password: '' };
  }

  ionViewDidLoad() {    
  }

  doLogin() {
    if (this.authService.validate(this.login)) {
      this.navCtrl.setRoot(HomePage);
    } else {
      this.notifyInvalid();
    }
  }

  notifyInvalid() {
    let toast = this.toast.create({
      message: 'Invalid email or password must be atleast 4 characters.',
      duration: 3000,
      position: 'bottom'
    });
    
    toast.present();
  }
}
