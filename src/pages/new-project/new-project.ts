import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { HomePage } from '../home/home';

import { Project } from '../../models/project';
import { ProjectServiceProvider } from '../../providers/project-service/project-service';

@Component({
  selector: 'page-new-project',
  templateUrl: 'new-project.html',
})
export class NewProjectPage {
  project: Project;
  projects: Project[];

  constructor(public navCtrl: NavController, private projectService: ProjectServiceProvider, private toast: ToastController) {
    this.project =  { name: '' };
  }

  ionViewDidLoad() {
    
  }

  async save() {
    if (!this.project.name) return;
    this.projects = await this.projectService.getAll() || [];

    if (!this.projects.find(x => x.name.toLowerCase() == this.project.name.toLowerCase())) {
      this.projects.push(this.project);
      this.projectService.add(this.projects);

      this.navCtrl.setRoot(HomePage);
    } else {
      this.notifyInvalid();
    }
  }

  notifyInvalid() {
    let toast = this.toast.create({
      message: 'Project name already exist.',
      duration: 3000,
      position: 'bottom'
    });
    
    toast.present();
  }
}
