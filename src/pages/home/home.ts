import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Project } from '../../models/project';
import { ProjectServiceProvider } from '../../providers/project-service/project-service';

import { NewProjectPage } from '../new-project/new-project';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  projects: Project[];

  constructor(public navCtrl: NavController, private projectService: ProjectServiceProvider) {
  }

  ionViewDidLoad() {
    this.getAll();
  }

  async getAll() {
    this.projects = await this.projectService.getAll() || [];    
  }

  addProject() {
    this.navCtrl.push(NewProjectPage);
  }
}
