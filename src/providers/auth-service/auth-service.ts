import { Injectable } from '@angular/core';
import { Login } from '../../models/login';

@Injectable()
export class AuthServiceProvider {

  constructor() {    
  }

  validate(login: Login) {
    let validEmailFormat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;    
    return validEmailFormat.test(login.email) && login.password.length >= 4;
  }
}
