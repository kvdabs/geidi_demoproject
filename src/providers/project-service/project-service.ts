import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { Project } from '../../models/project';

@Injectable()
export class ProjectServiceProvider {

  constructor(private storage: Storage) {    
  }

  getAll = () => {
    return this.storage.get('projects');
  }

  add = (projects: Project[]) => {    
    this.storage.set("projects", projects);
  }
}
